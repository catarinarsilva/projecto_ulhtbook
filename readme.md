FICHEIROS:

- Nome da aplicação : ulhtbook

- Existem 3 tipos de utilizador sendo que o administrador tem como login(Admin@pt) e como password (1234).

- Pagina Principal (index.php):
	Este ficheiro é a pagina inicial da aplicação, que é onde podemos fazer login, registarmo-nos(registar.php) e consultar todos os utilizadores existentes(lista_users.php).

- Perfil de utilizador (perfil.php):
	Este ficheiro mostra a perfil com todas informações (nome, idade, email, aniversário, estado civil, localização, escola que frequentou, local onde trabalha, etc) do utilizador (user)  que realizou o login.
	Esta pagina também possui um conjunto de Funcionalidades que o utilizador pode realizar como :
				
				- Apagar (apagar.php) : este ficheiro tem como função apagar o perfil do user logado, sendo que o utilizador não pode apagar o seu proprio perfil.
				
				- Editar (editar.php) : este ficheiro permite editar todas as informações do utilizador logado.
				
				- Consultar a lista de todos os utilizadores (Lista_utilizadores.php): permite consultar a lista de todos os utilizadores registados na aplicação. 
				
				- Consultar as suas amizades (lista_amigos) : este ficheiro permite ver todos os amigos do utilizador logado e consultar os seus perfis.
				
				- Aceitar as amizades pedentes (pedidos_amizade) : neste ficheiro o utilizador pode aceitar ou rejeitar os pedidos de amizade envolve também o ficheiro (aceitar_amizade.php) para realizar a operação requerida.
				
				- Pedir amizade a outros utilizadores (pedidos_novos_amigos) : neste ficheiro podemos consultar a lista de todos os utilizadores da aplicação, e pedir amizade esta operação também envolve o ficheiro (efetuar_pedido).

- Perfil de administrador (lista_admin):

	Este ficheiro mostra o perfil do administrador com todas as suas informações, podendo este edita-las, a lista de todos os utilizadores, como também todas as funções que este pode efectuar sobre eles (Criar um novo utilizador, editar/apagar a conta de utilizadores registados).


BASE DE DADOS:

	- O nosso trabalho possui uma base de dados com o nome ulhtbook , sendo esta constituida por 2 tabelas:
				- A tabela amigos é constituida por 3 colunas ( id, id_amigo, estado)
				- A tabela users é constituida por 11 colunas ( login, password, id, nome, idade, localização, foto_de_perfil, estado_civil, onde_estudou, onde_trabalha, data_nascimento).




