-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15-Dez-2016 às 16:05
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ulhtbook`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `login` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `id` int(11) NOT NULL,
  `nome` text,
  `idade` int(11) DEFAULT NULL,
  `localizacao` text,
  `foto_de_perfil` text,
  `estado_civil` text,
  `onde_estudou` text,
  `onde_trabalha` text,
  `data_nascimento` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`login`, `password`, `id`, `nome`, `idade`, `localizacao`, `foto_de_perfil`, `estado_civil`, `onde_estudou`, `onde_trabalha`, `data_nascimento`) VALUES
('Admin@pt', '1234', 43, 'Administrador', 12, '', 'https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjE1Ma9qvbQAhUD0xoKHZJQB14QjRwIBw&url=https%3A%2F%2Fwww.mensagenscomamor.com%2Fimagens-de-deus&psig=AFQjCNGhqPvExzX_0IcPmSmn4kzwLE2x3g&ust=1481895839580398', '', '', '', ''),
('ricardo@pt', '123', 45, 'Ricardo', NULL, NULL, 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQKbLAe0x4wEO_SPki4NqCk-aSv-NrgVzI-vKU9VJLxuhK-RLRtdblUfvbI', NULL, NULL, NULL, NULL),
('cro@pt', '123', 41, 'Catarina', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('123@com', '000', 42, 'n', 0, '', '', '', '', '', ''),
('porto@pt', '1234', 47, 'Porto', 0, '', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQwfCqCRB55_z-kxuQJpkaPwIJ4DyWZ8ZGaV-NoqxhiLC0--O5C', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
